<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Process {

    public function entry_preview($entryID)
    {
    	//Assigns codeigniter resources to the $CI object
    	$CI =& get_instance();
    	
		$processed_entry = array();
		
		//entry_authorization checks that an entry with the supplied id exists and the author_id matches the currently logged in user's author_id
		//returns 1 for authorized and 0 for no authorization
		$auth = $CI->entries_model->entry_authorization($entryID, $CI->session->userdata('author_id'));
		
		if($auth > 0)
		{
			//flow control counter
			$counter = 0;
			
			//since authorization is handled already only the entryID is required
			$result = $CI->entries_model->get_entry($entryID);
			
			//Create a new dom object and load the html that will be parsed
			$bodyDOM = new domDocument;
			
			//in this case it is the body of the returned entry
			//The @ is required because otherwise it throws an error due to malformed html
			@$bodyDOM->loadHTML($result[0]['body']);
			
			$bodyDOM->preserveWhiteSpace = FALSE;
			$images = $bodyDOM->getElementsByTagName('img');
			
			if($images->length > 0)
				{
				//Declare the array that will store the scraped images	
				$image_array = array();
				
				//This extracts all usable information from all images in the post
				foreach ($images as $image) {
  					$image_array[$counter]['src'] 		= $image->getAttribute('src');
					$image_array[$counter]['alt'] 		= $image->getAttribute('alt');
					$image_array[$counter]['height']	= $image->getAttribute('height');
					$image_array[$counter]['width']		= $image->getAttribute('width');
					$counter++;
					}
				}
				
			else{$image_array = 0;}
			
			//Assemble the post array
			$processed_entry['body'] = strip_tags($result[0]['body']);
			$processed_entry['images'] = $image_array;
		}
		
		Return $processed_entry;
    }
	
}

/* End of file process.php */
