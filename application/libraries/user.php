<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User {

    public function account_data()
    {
    	//Assigns codeigniter resources to the $CI object	
    	$CI =& get_instance();
    	$CI->load->model('userinfo_model');
		$results = $CI->userinfo_model->getUser_info($CI->session->userdata('user_id'));
		Return $results;
    }
	
	public function pub_data()
	{
		//Assigns codeigniter resources to the $CI object
		$CI =& get_instance();
		$results = $CI->userinfo_model->get_publicationsID($CI->session->userdata('user_id'));
		Return $results;
	}
	
	
}

/* End of file process.php */
