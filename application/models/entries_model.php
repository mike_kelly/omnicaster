<?php
class Entries_model extends CI_Model {
	
	function update_entry($feed_id, $entry_id, $data)
	{
		$this->db->where('feed_id', $feed_id);
		$this->db->where('id', $entry_id);
		$this->db->update('feed_entries', $data);
	}

	function entry_authorization($entryID, $authorID)
	{
		$this->db->from('feed_entries');
		$this->db->where('id', $entryID);
		$this->db->where('author_id', $authorID);
		
		Return $this->db->count_all_results();
	}
	
	function get_entry($entryID)
	{
		$this->db->from('feed_entries');
		$this->db->where('id', $entryID);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function create_entry($data)
	{
		$this->db->insert('feed_entries', $data);
		return $this->db->affected_rows();
	}
	
}
