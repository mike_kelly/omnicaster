<?php
class Login_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function validate_login($email, $passHash)
    {
		//$query = $this->db->get_where('Users', array('email' => $loginData['email'], 'passHash'	=> $loginData['passHash']));
		$this->db->where('email', $email);
		$this->db->where('passHash', $passHash);
		$query = $this->db->get('Users');
        if($query->num_rows() > 0)
		{
			$row = $query->row_array();
			 $this->session->set_userdata('accountID', $row['accountID']);	
		}
		Return $query->num_rows();
    }
}