<?php
class Feeds_model extends CI_Model {
	
	function get_feeds($author_id, $pub_id)
	{
		$this->db->select('*');
		$this->db->from('feeds');
		$this->db->like('author_id', $author_id);
		$this->db->where('publication_id', $pub_id);
		//$this->db->join('feed_entries','feeds.id = feed_entries.feed_id');
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function get_feed($author_id, $feed_name)
	{
		$this->db->select('id');
		$this->db->from('feeds');
		$this->db->like('author_id', $author_id);
		$this->db->where('name', $feed_name);
		$this->db->limit(1);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function get_entries($feedID, $returnLimit = 'none', $offset = 0)
	{
		$this->db->select('*');
		$this->db->from('feed_entries');
		$this->db->where('feed_id', $feedID);
		
		if($returnLimit != 'none'){$this->db->limit($returnLimit, $offset);}
		
		$query = $this->db->get();
		
		Return $query;
	}
	
	function get_entry($feedID, $entryID)
	{
		$this->db->select('*');
		$this->db->from('feed_entries');
		$this->db->where('feed_id', $feedID);
		$this->db->where('id', $entryID);
		
		$query = $this->db->get();
		
		Return $query;
	}
	
	
	function feed_authorization($feedID, $pubID, $authorID)
	{
		$this->db->from('feeds');
		$this->db->where('id', $feedID);
		$this->db->where('publication_id', $pubID);
		//Author IDs will be concantinated with one another as colon seperated values (xxx:xxx:xxx)
		$this->db->like('author_id', $authorID);
		
		Return $this->db->count_all_results();
	}
	
	function get_feedName($feedID)
	{
		$this->db->from('feeds');
		$this->db->where('id', $feedID);
		$query = $this->db->get();
		Return $query->row();
	}
}