<?php
class Settings_model extends CI_Model {
	
	function get_settings()
	{
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->from('users');
		
		$query = $this->db->get();
		
		Return $query->row();
	}
	
	function update_settings($dbFieldName, $sessionFieldName, $value)
	{
		$data = array($dbFieldName => $value);

		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->update('users', $data); 
		
		$newdata = array($sessionFieldName  => $value);

		$this->session->set_userdata($newdata);
	}
	
}