<?php
class Subscribers_model extends CI_Model {
	
	function get_subscribers($feed_id)
	{
		$this->db->select('email');
		$this->db->from('subscribers');
		$this->db->like('feeds', $feed_id);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function subscribers_suggest($email)
	{
		$this->db->select('*');
		$this->db->from('subscribers');
		$this->db->like('email', $email);
		$query = $this->db->get();
		
		Return $query;
	}
}