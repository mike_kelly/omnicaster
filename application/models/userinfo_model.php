<?php
class Userinfo_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getUser($id)
    {
		//$query = $this->db->get_where('Users', array('email' => $loginData['email'], 'passHash'	=> $loginData['passHash']));
		$this->db->where('id', $id);
		$query = $this->db->get('users');
        
		Return $query->result_array();
    }
	
	function getUser_info($id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('users.id', $id);
		$this->db->join('users_groups', 'users.id = users_groups.user_id');
		$this->db->join('groups', 'users_groups.group_id = groups.id');
		$this->db->join('users_preferences', 'users.id = users_preferences.user_id');
		$query = $this->db->get();
		
		Return $query->result_array();
	
	}
	
	function get_publicationsID($author_id)
	{
		$this->db->select('*');
		//$this->db->select('publication_id');
		$this->db->from('users');
		$this->db->where('users.id', $author_id);
		$this->db->join('feeds','users.author_id = feeds.author_id');
		$this->db->join('publications', 'feeds.publication_id = publications.id');
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function get_publicationInfo($pubID)
	{
		$this->db->select('name');
		$this->db->from('publications');
		$this->db->where('id', $pubID);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	

	
}