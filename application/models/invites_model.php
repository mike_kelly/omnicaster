<?php
class Invites_model extends CI_Model {
	
	function put_invitees($data)
	{
		$this->db->insert('invites', $data);
		
		Return $this->db->affected_rows(); 
	}
	
	function get_invitees($user_id, $pub_id)
	{
		$this->db->select('*');
		$this->db->from('invites');
		$this->db->where('userID',$user_id);
		$this->db->where('pubID',$pub_id);
		$this->db->order_by('inviteTime', "asc");
		$query = $this->db->get();
		
		Return $query;
	}
	
	function del_invite($email, $md5Time)
	{
		$this->db->delete('mytable', array('id' => $id));
		Return $this->db->affected_rows();
	}
	
	function lookupInvitee($email, $md5Time)
	{
		$this->db->where('email', $email);
		$this->db->where('inviteHash', $md5Time);
		$query = $this->db->get('invites');
		
		Return $query->row();
	}
}