<?php
class Invites_model extends CI_Model {
	
	function get_feeds($author_id, $pub_id)
	{
		$this->db->select('*');
		$this->db->from('feeds');
		$this->db->where('author_id', $author_id);
		$this->db->where('publication_id', $pub_id);
		//$this->db->join('feed_entries','feeds.id = feed_entries.feed_id');
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function get_entries($feed_id)
	{
		$this->db->select('*');
		$this->db->from('feed_entries');
		$this->db->where('feed_id', $feed_id);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
}