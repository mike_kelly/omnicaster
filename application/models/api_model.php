<?php
class Api_model extends CI_Model {
	
	function get_feedID($feed_name)
	{
		$this->db->select('id');
		$this->db->from('feeds');
		$this->db->like('name', $feed_name);
		$query = $this->db->get();
		
		Return $query->result_array();
	}
	
	function get_feedEntries($authorID, $feedID)
	{
		$this->db->where('author_id', $authorID);
		$this->db->where('feed_id', $feedID);
		$this->db->limit(10);
		$this->db->order_by('publish_date', 'desc');
		$query = $this->db->get('feed_entries');
		Return $query->result_array();
	}
	
	function get_authorName($authorID)
	{
		$this->db->select('first_name, last_name');
		$this->db->where('author_id',$authorID);
		$this->db->limit(1);
		$query = $this->db->get('users');
		Return $query->row();
	}
	
	function get_article($authorID, $feedID, $articleID)
	{
		$this->db->select('body,title');
		$this->db->where('author_id',$authorID);
		$this->db->where('feed_id', $feedID);
		$this->db->where('id', $articleID);
		$this->db->limit(1);
		$query = $this->db->get('feed_entries');
		Return $query->row();
	}
	
	function feed_EntriesByID($feedID, $dateToday)
	{
		$this->db->where('feed_id', $feedID);
		$this->db->where('publish_date <', $dateToday);
		$this->db->limit(10);
		$query = $this->db->get('feed_entries');
		Return $query->result_array();
	}
	
	function feed_EntriesByAuthor($authorID, $dateToday)
	{
		$this->db->where('author_id', $authorID);
		$this->db->where('publish_date <', $dateToday);
		$this->db->limit(10);
		$query = $this->db->get('feed_entries');
		Return $query->result_array();
	}
	
	function feed_EntryByID($EntryID, $dateToday)
	{
		$this->db->where('id', $EntryID);
		$this->db->where('publish_date <', $dateToday);
		$query = $this->db->get('feed_entries');
		Return $query->row();
	}
}