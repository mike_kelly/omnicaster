	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span8 offset2">
				
				<!-- Main content area -->
				<h2>Feed Management</h2>
				<!-- <button class="btn btn-mini btn-primary pull-right disabled feed-action" disabled id="new" type="button">New Feed</button> -->
				<hr />
				<div id="accordion">
					<?php
					foreach($publication['feeds'] as $feeds)
						{ $count++; ?>
						<h3><?php echo $feeds['name']; ?><span class="pull-right">Entries: <?php if($feeds['entries'] == 0){echo 0;}else{echo count($feeds['entries']);}?></span></h3>
						<div>
							<h4><?php echo $feeds['description']; ?></h4>
							<form method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>subscribers" />
								<input type="hidden" name="feed-name" value="<?php echo $feeds['name'];?>">
								<h5>Manage: </h5><div class="btn-group"><button class="btn disabled" disabled id="<?php echo $feeds['name'];?>" type="submit">Subscribers</button> <?php if($feeds['entries'] != 0){ ?> <button class="btn manage feed-action" id="<?php echo $feeds['name'];?>" type="button">Entries</button><?php } else{ ?><button class="btn btn-primary create feed-action" id="<?php echo $feeds['name']; ?>" type="button">New Entry</button> <?php } ?></div>
  							</form>
						</div>
					<?php } ?>
				</div>
				
			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->	


	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	
	<script>
	    $('.textarea').wysihtml5();
	</script>
	
	
	