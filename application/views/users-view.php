	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span8 offset2">
				
				<!-- Main content area -->
				<h2>User Management</h2>
				<div id="tabs">
				
  					<ul>
    					<li><a href= "#tabs-1" >Publication Users</a></li>
    					<li><a href= "#tabs-2" >Invite User</a></li>
  					</ul>
				
  					<div id="tabs-1">
    					<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
  					</div>
				
  					<div id="tabs-2">
  						<form id="inviteesForm">
  							<strong>Invitee Email Address: </strong><input id="email" type="text" class="input-xlarge">
  							<label class="checkbox pull-right">
   								<input id="addto" name="addto" type="checkbox"> Add user to current publication.
  							</label>
  							<br />
  							<button id="addButton" name="addButton" type="button" class="btn btn-small pull-right" tabindex=23>Submit</button>
  						</form>
  						<br />
  						<hr />
						<div id="userInvite">
							<h3>No user invites found.</h3>
						</div>
					</div>
				</div>
				 
			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->
	
	<div id="dialog" title="Cookie Monster Says">
		
	</div>
	
	
	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	<script>
	    $('.textarea').wysihtml5();
	</script>
	
	<script type="text/javascript" charset="utf-8">
	    $(prettyPrint);	
	</script>