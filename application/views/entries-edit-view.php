	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span8 offset2">
				
				<!-- Main content area -->
				<?php echo form_open('save/entry'); ?>
					<h2>Edit Entry</h2>
					<table class="table">
						<tr>
							<td>
	   	 						<label class="control-label" for="entrytitle">Title</label>
	    						<input class="input-xxlarge" type="text" name="entrytitle" id="entrytitle" value="<?php echo $entry_data[0]['title']; ?>">
	  						</td>
	  						<td align="right">
 								<label class="control-label" for="pubdate">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Publish Date</label>
   								<input class="input-medium pull-right" type="text" name="pubdate" id="pubdate" value="<?php echo $entry_data[0]['publish_date']; ?>">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<textarea class="field" rows="18" cols="5" name="entrybody" id="entrybody"><?php echo $entry_data[0]['body']; ?></textarea>
							</td>
						</tr>
					</table>
					<!-- hidden inputs-->
					<input type="hidden" name="entryid" id="entryid" value="<?php echo $entry_data[0]['id']; ?>" />
					<input type="hidden" name="feedid" id="feedid" value="<?php echo $entry_data[0]['feed_id']; ?>" />
					
					<div class="btn-group pull-right"><button class="btn btn-danger" type="button" id="cancel">Cancel</button> <button class="btn btn-primary" id="submit">Submit</button></div>
					<!-- <button id="submit" class="btn btn-primary pull-right">Submit</button> -->
				</form>
				
			</div><!--/span-->
		</div><!--/row-->
		<hr />
	</div><!--/.fluid-container-->
	
	
	<script type="text/javascript" src="<?php echo base_url(); ?>/resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" charset="utf-8">
	    $(prettyPrint);
	</script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>resource/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		//for config information goto:
		//http://www.tinymce.com/wiki.php/Controls
		tinymce.init({
    		selector: "textarea",
    		plugins: "image",
    		height: "300",
    		toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			});
	</script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>resource/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript">
		//for config information goto:
		//http://trentrichardson.com/examples/timepicker/
		
		 $("#pubdate").datepicker(
        {
            dateFormat: 'yy-mm-dd',
            //showOn: 'button',
            //buttonImage: '../chassis/images/calendar.gif',
            //buttonImageOnly: true,
            //minDate: 0,
            //maxDate: '+5Y',
            //duration: '',
            //<c:if test="${formIsReadonly or form.newsItemId == '-1'}">disabled: true,</c:if>
            //constrainInput: false,
            timeSuffix: ':00:00',
            timeFormat: 'hh TT'  // time format here
        });
        
	</script>