<!-- Beggining of Navbar -->
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container-fluid">
            
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <a class="brand" href="<?php echo base_url();?>"><?php echo $this->session->userdata('publication');?></a>
			
            <div class="nav-collapse">
                
                <ul class="nav">
                    <!-- <li class="<?php if($menuSelect == "main"){echo "active";} ?>"><a href="<?php echo base_url('admin');?>">Home</a></li> -->
  					<!-- <li class="<?php if($menuSelect == "users"){echo "active";} ?>"><a href="<?php echo base_url('users');?>">Users</a></li> -->
  					<!-- <li class="<?php if($menuSelect == "subscribers"){echo "active";} ?>"><a href="<?php echo base_url('manage/subscribers'); ?>">Subscribers</a></li> -->
  					<!-- <li class="<?php if($menuSelect == "feeds"){echo "active";} ?>"><a href="<?php echo base_url('manage/feeds'); ?>">Feeds</a></li>-->
  					<!-- <li class="<?php if($menuSelect == "schedules"){echo "active";} ?>"><a href="#">Schedules</a></li> -->
                </ul>
				
				<form class="navbar-search pull-right">
  					<input type="text" class="search-query" placeholder="Search">
				</form>
				
                <ul class="nav pull-right">
                    
                    <li class="divider-vertical"></li>
                    
                    <li>
                        <ul class="nav">
                            
                            <li class="dropdown">
                                <a href="#"
                                   class="dropdown-toggle"
                                   data-toggle="dropdown">
                                    <i class="icon-user"></i> <?php echo $userdata[0]['first_name'].' '.$userdata[0]['last_name'];?>
                                    <b class="caret"></b>
                                </a>
                                
                                <ul class="dropdown-menu">

                                    <li>
                                    	
                                        <div style="width: 314px">
                                            
                                            <div class="modal-header">
                                                <h4><?php echo $userdata[0]['first_name'].' '.$userdata[0]['last_name'];?> - Administrator</h4>
                                            </div>
                                            
                                            <div class="modal-body">
                                                <div class="row-fluid">
                                                    <div class="span3">
                                                        <img src="<?php echo base_url(); ?>resource/img/defaultPhoto.png" alt="" class="thumbnail span12">
                                                    </div>
                                                    <div class="span9">
														<a href= "#myModal"  role="button" data-toggle="modal"><i class="icon-cogs"></i> Account Settings</a>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button id="logoutbtn" class="btn btn-primary"><i class="icon-off"></i> Log Out</button>
                                            </div>
                                            
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            
                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>

<!-- End of Navbar -->
