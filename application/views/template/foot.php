	<?php $this->load->view('scripts/'.$script); ?>

	


	<!-- Modal -->
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Account Settings</h3>
		</div>
		<form class="form-horizontal" method="post" accept-charset="utf-8" action="<?php echo base_url('settings'); ?>" />
			<div class="modal-body">
				
				<div class="control-group">
    				<label class="control-label" for="inputEmail">Email:</label>
    				<div class="controls">
    					<h5 id="valueEmail" style="margin-top: 5px; cursor: pointer;"><?php echo $this->session->userdata('email'); ?></h5>
      					<input type="text" style="display: none;" name="inputEmail" id="inputEmail" placeholder="Email">
    				</div>
  				</div>
  				
  				<div class="control-group">
    				<label class="control-label" for="inputEmail">First Name:</label>
    				<div class="controls">
      					<h5 id="valueFirstName" style="margin-top: 5px; cursor: pointer;">James</h5>
      					<input type="text" style="display: none;" name="inputFirstName" id="inputFirstName" placeholder="First Name">
    				</div>
  				</div>
  				
  				<div class="control-group">
    				<label class="control-label" for="inputEmail">Last Name:</label>
    				<div class="controls">
    					<h5 id="valueLastName" style="margin-top: 5px; cursor: pointer;">Chilton</h5>
      					<input type="text" style="display: none;"name="inputLastName" id="inputLastName" placeholder="Last Name">
    				</div>
  				</div>
  				<button class="btn btn-link pull-right" id="changePassword" type="button"><i class="icon-cog"></i> Change Password</button>
				<input type="hidden" name="sourceURL" id="sourceURL" value="<?php echo current_url(); ?>">
				
			</div>
			<div class="modal-footer">
				<button class="btn" type="button" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" type="submit">Save changes</button>
			</div>
		</form>
	</div>
	
	<script type="text/javascript" charset="utf-8">
		//if button is clicked
		$("#logoutbtn").click(function(){
			window.location.href = "<?php echo base_url(); ?>auth/logout";
			});
			
		$(document).ready(function(){
			
			$("#inputEmail").val($("#valueEmail").text());
			$("#inputFirstName").val($("#valueFirstName").text());
			$("#inputLastName").val($("#valueLastName").text());
			
			
			$("#valueEmail").click(function(){
				email = $("#valueEmail").text();
				
				$("#valueEmail").hide();
				$("#inputEmail").val(email);
				$("#inputEmail").show();
				});
			
			$("#valueFirstName").click(function(){
				firstName = $("#valueFirstName").text();
				
				$("#valueFirstName").hide();
				$("#inputFirstName").val(firstName);
				$("#inputFirstName").show();
				});
			
			$("#valueLastName").click(function(){
				lastName = $("#valueLastName").text();
				
				$("#valueLastName").hide();
				$("#inputLastName").val(lastName);
				$("#inputLastName").show();
				});
			
			$("#changePassword").click(function(){
				window.location.href = "<?php echo base_url(); ?>auth/change_password";
				});
			
			});
	</script>
</body>
</html>