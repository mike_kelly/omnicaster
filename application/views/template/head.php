<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title ;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->

    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>bootstrap/css/font-awesome.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>resource/css/style.css" type="text/css" rel="stylesheet">
 	<link href="<?php echo base_url(); ?>resource/css/blitzer/jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet">
 	<link href="<?php echo base_url(); ?>resource/css/custom.css" type="text/css" rel="stylesheet">

    <style type="text/css">
        body {
            padding-top: 160px;
            padding-bottom: 40px;
            background-image: url("<?php echo base_url(); ?>resource/img/bg.png");
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        
        #ui-datepicker-div 
		{
			font-size:12px; 
		}
    </style>
    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="bootstrap/js/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
	
    <link rel="shortcut icon" href="<?php echo base_url(); ?>bootstrap/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>bootstrap/ico/bootstrap-apple-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>bootstrap/ico/bootstrap-apple-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>bootstrap/ico/bootstrap-apple-72x72.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>/bootstrap/icobootstrap-apple-57x57.png">
	
    <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/js/jquery-ui-1.10.3.custom.min.js"></script>
    
</head>
<body>