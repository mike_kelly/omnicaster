	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span8 offset2">
				<!-- Main content area -->
				
				<!-- Beginning of Chart -->
				<div class="well">
					<div id="chart"></div>
				</div>
				<!-- End of Chart -->
			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->

	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	
	<script>
	    $('.textarea').wysihtml5();
	</script>

	<script type="text/javascript" charset="utf-8">
	    $(prettyPrint);
	</script>
