	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span8 offset2">
				<!-- Main content area -->
				<h2><?php echo $feedTitle; ?></h2>	
				<div class="btn-group pull-right entry-buttons">
  					<button id="manage-feeds" class="btn btn-small btn-primary">Manage Feed</button>
  					<button id="newEntry" class="btn btn-small btn-success">New Entry</button>
				</div>
				
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th></th>
						<th>Title</th>
						<th align="right">Content</th>
						<th>Publish Date</th>
						<th></th>
					</tr>
				<?php 	
					foreach($entries as $row)
					{
						echo
							'<tr>
								<td class="main-row" style="line-height: 10px;">
									<span class="arrow-span"><i class="icon-arrow-down icon-white"></i> <i class="icon-arrow-up icon-white" style="display: none;"></i></span>
								</td>
								<td class="main-row" style="line-height: 10px;">
									<h5>'.$row['title'].'</h5>
								</td>
								<td class="main-row" style="line-height: 10px;">'
									.$row['content'].
								'</td>
								<td class="main-row" style="line-height: 10px;">'
									.$row['publish_date'].
								'</td>
								<td class="main-row" style="line-height: 10px;">'
									.$row['actions'].
								'</td>
							</tr>
							<tr class="options" style="display: none;">
								<td colspan=5 style="height: 100px;"><h4>Placeholder for planned features.</td>
							</tr>';
					}
						
					
					
					
					
					//echo $this->table->generate($entries);
					//echo $this->uri->segment(3);
					//echo '<br />';
					//print_r($entries); 
					
				?>
				</table>
						<?php echo $this->pagination->create_links(); ?>


			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->
	
	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	
	<script>
	    $('.textarea').wysihtml5();
	</script>

	<script type="text/javascript" charset="utf-8">
	    $(prettyPrint);
	</script>