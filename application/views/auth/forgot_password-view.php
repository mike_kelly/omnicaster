
<body>
<div class="container">
    <div class="row">
        <div class="span4 offset4">
				<!-- Main content area -->                        <a class="brand" href="#" style="color: #ffffff;"><?php echo lang('forgot_password_heading');?></a>
                        <div class="nav-collapse">
                        </div><!-- /.nav-collapse -->
           
           
            
				<?php echo form_open("auth/forgot_password",array('class' => 'well'));?>
					<h5><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></h5>
					<br />
					<div class="input-prepend">
						<span class="add-on"><i class="icon-envelope"></i></span>
						<input class="span3" type="text" name="email" id="email" value="" placeholder="Email Address" />
					</div>
					<p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>
				<?php echo form_close();?>
			<div class="alert alert-error" id="error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			    <div id="infoMessage"><?php echo $message;?></div>
			</div>
			<div class="alert alert-error" id="error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			    <div id="infoMessage"><?php echo $message;?></div>
			</div>
        </div>
    </div>
</div>
</body>
