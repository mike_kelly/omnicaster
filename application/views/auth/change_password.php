<?php 
	$data = array(
		'title'		 	=> 	'Manage Subscribers',
		'menuSelect'	=>	'subscribers',
		'script'		=>	'subscribers-script'
		);
	
	$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
	$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
	
	$this->load->view('template/head', $data);
	$this->load->view('template/navbar', $data);?>
	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span3 offset4">
				<!-- Main content area -->
				<h2><?php echo lang('change_password_heading');?></h2>

				<div id="infoMessage"><?php echo $message;?></div>



				<?php 
					$attributes = array('class'=>'form-horizontal');
					echo form_open("auth/change_password");
				?>

      			
            	<h4><?php echo lang('change_password_old_password_label', 'old_password');?></h4>
            	<?php echo form_input($old_password);?>
      			

      			
            	<h4><label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label></h4>
            	<?php echo form_input($new_password);?>
      			

      			<h4><?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?></h4>
            	<?php echo form_input($new_password_confirm);?>
      			
      			<?php echo form_input($user_id);?>
      			<br />
      			<br />
      			<div class="btn-group pull-right"><button class="btn btn-danger" type="button" id="cancel">Cancel</button> <button class="btn btn-primary" type="submit">Submit</button></div>
      			

				<?php echo form_close();?>


			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->
	
	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
						
			$("#cancel").click(function(){
			window.location.href = "<?php echo base_url(); ?>";
			});
				
			
			});
	</script>
	
<?php  $this->load->view('template/foot', $data);?>