<?php $this->load->view('template/head');?>
<body>
<div class="container">
    <div class="row">
        <div class="span4 offset4">
			<h3>Omnicaster</h3>
			<h4>Have a voice without the crowd.</h4>
			<?php echo form_open("auth/login",array('class' => 'well'));?>
				<div class="input-prepend">
					<span class="add-on"><i class="icon-envelope"></i></span>
					<input class="span3" name="identity" type="text" value="" placeholder="Email Address" id="identity"  />
				</div>
			
				<div class="input-prepend">
					<span class="add-on"><i class="icon-key"></i></span>
					<input class="span3" name="password" type="password" value="" placeholder="Password" id="password"  />	
				</div>
			
				<label class="checkbox">
					<?php echo form_checkbox(array('name' => 'remember','id' => 'remember','value' => 'accept','checked' => FALSE,'style' => 'display:inline'));?>
					Remember me
				</label>
				<p class="pull-right"><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
				<p><input type="submit" name="submit" value="Login" class="btn btn-danger"  /></p>

			<?php echo form_close();?>

			<div class="alert alert-error" id="error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			    <div id="infoMessage"><?php echo $message;?></div>
			</div>
        </div>
    </div>
</div>
</body>
