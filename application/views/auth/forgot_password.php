<?php 
	$data = array(
		'title'		 	=> 	'Manage Subscribers',
		'menuSelect'	=>	'subscribers',
		'script'		=>	'subscribers-script'
		);
	
	$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
	$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
	
	$this->load->view('template/head', $data);
	$this->load->view('template/navbar', $data);?>
	<div class="container-fluid pulled-up">
		<div class="row-fluid">
			<div class="span3 offset4">
				<!-- Main content area -->
				<h1><?php echo lang('forgot_password_heading');?></h1>
				<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

				<div id="infoMessage"><?php echo $message;?></div>

				<?php echo form_open("auth/forgot_password");?>

		      	<p>
      				<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
      				<?php echo form_input($email);?>
      			</p>

      			<p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

				<?php echo form_close();?>
				
			</div><!--/span-->
		</div><!--/row-->
		<hr>
	</div><!--/.fluid-container-->
	
	<script type="text/javascript" src="../resource/chart/chart.js"></script>
	<script type="text/javascript" src="../resource/chart/highcharts.js"></script>
	<script type="text/javascript" src="../resource/chart/exporting.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/prettify.js"></script>
	<script type="text/javascript" src="../resource/wysi/js/bootstrap-wysihtml5.js"></script>
	
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
						
			$("#cancel").click(function(){
			window.location.href = "<?php echo base_url(); ?>";
			});
				
			
			});
	</script>
	
<?php  $this->load->view('template/foot', $data);?>