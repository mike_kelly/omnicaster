<script type="text/javascript"> // users-script.php

$(function() {
    $("#tabs").tabs().css({
   'min-height': '550px',
   'overflow': 'auto'
	});
});

var ajaxloader ="<img src='<?php echo base_url(); ?>resource/img/ajax_loader.gif' alt='loading...' />";
var loadUrl = "http://omnicaster.plusonedevelopments.com/users/getinvites/";  
    			 
$("#userInvite").html(ajaxloader).load(loadUrl);

//dialog box confirmation setup
$(function() {
	$("#dialog").dialog({
		modal: true,
		autoOpen: false,
		resizable: false,
		width: 400,
		buttons: {
			"OK": function() {$(this).dialog("close");},
			}
		});
	});

$(document).ready(function() {
	
 	$('#addButton').click(function()
		{
		email = $("#email").val();
		
		if ($('#addto').is(':checked')) {addToPub = 1;} 
		else {addToPub = 0;}
		
		// add variables to datastring
		// email inviteTime userID pubID addToPub
		var dataString = 'email=' + email + '&addToPub=' + addToPub;
		
		// {url, data, success(data, textStatus, jqXHR), dataType}
		$.post("http://omnicaster.plusonedevelopments.com/users/invitees/",dataString,function(result)
			{
				//$("#userInvite").html(result);
				//change html for the dialog modal by passing in result
				$("#dialog").html(result);
				$("#dialog").dialog("open");
				
				var loadUrl = "http://omnicaster.plusonedevelopments.com/users/getinvites/";  
				
    			//var ajax_load = "<img src='img/load.gif' alt='loading...' />"; 
        		$("#userInvite").html(ajaxloader).load(loadUrl);
				
				//reset form to init state
				$("#email").val("");
				$("#addto").removeAttr('checked');
				
			},"html");
		
		}); //End of .click function
	
	}); //End of document.ready


	
	
	
</script>