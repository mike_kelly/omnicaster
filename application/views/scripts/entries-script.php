<script type="text/javascript"> // entries-script.php

$(document).ready(function() {
	

	
	$('.delete, .edit').css('cursor', 'pointer');

	
	$(".edit").click(function() {
  		data = "<?php echo base_url(); ?>manage/entries/edit/"+$(this).closest('span').attr('id');
  		//alert(data);
  		window.location.href = data;
		});
	
	
	
	$(".email").click(function() {
  		data = "<?php echo base_url(); ?>manage/entries/email/"+$(this).closest('span').attr('id');
  		//window.location.href = data;
  		alert(data);
		});
	
	$(".qrcode").click(function() {
  		data = "<?php echo base_url(); ?>manage/entries/qrcode/"+$(this).closest('span').attr('id');
  		//window.location.href = data;
  		alert(data);
		});
		
	$(".share").click(function() {
  		data = "<?php echo base_url(); ?>manage/entries/share/"+$(this).closest('span').attr('id');
  		//window.location.href = data;
  		alert(data);
		});
	//edit
	//share
	//qrcode
	//email
	
	$("#newEntry").click(function(){
		var pathname = window.location.pathname;
		var entryID = pathname.substr(pathname.lastIndexOf('/') + 1);
		
		data = "<?php echo base_url(); ?>create/entry/"+entryID;
		//alert(data);
  		window.location.href = data;
		
		});
	
	$("#manage-feeds").click(function(){
		data = "<?php echo base_url(); ?>manage/feeds";
  		//alert(data);
  		window.location.href = data;
		});
	
	
	
	
	$('.options').hide();
	
	$(".arrow-span").hover().css('cursor', 'pointer');
	
	$(".arrow-span").click(function() {
		if($(this).children('i:last').css('display') == 'none')
  			{
  			$(this).children('i:first').hide();
  			$(this).children('i:last').show();
  			$(this).closest('tr').next('tr').slideDown();
  			//$(this).closest('tr').siblings('tr.options').slideUp();
  			}
  		else
  			{
  			$(this).children('i:last').hide();
  			$(this).children('i:first').show();
  			$(this).closest('tr').next('tr').hide();
  			}
		});
	
	});
	
</script>