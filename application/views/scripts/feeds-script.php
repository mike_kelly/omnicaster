<script type="text/javascript"> // feeds-script.php
$(document).ready(function() {
	
	//Configure tables for row clicking
	// #example td:hover {cursor: pointer;}
	$('td').mouseover().css({ "cursor": "pointer"});
	
	$(prettyPrint);
		
	$("#accordion").accordion({collapsible : true, active : 0});
	
	
	
	$(".feed-action").click(function(){
    	
    	var id = this.id;
		if ($(this).hasClass('manage'))
  			{
  			
  			$.get('<?php echo base_url(); ?>api/linkgen/manage/'+encodeURIComponent(id), function(data) {
  				window.location.href = data;
  				//alert(data);
				});
  			}
		else
	  		{
  			$.get('<?php echo base_url(); ?>api/linkgen/create/'+encodeURIComponent(id), function(data) {
  				window.location.href = data;
  				//alert(data);
				});
  			}
	});
	
	
	});
	
	
</script>