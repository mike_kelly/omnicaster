<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller
	{
	public function index()
		{
		show_error('Sorry you are not Authorized to view this page!', 403);
		}
	
	public function linkGen()
		{
		
		$action = $this->uri->segment(3);
		$feedName = $this->uri->segment(4);
		
		switch ($action) {
			case 'manage':
        		$baseURL = base_url().'manage/';
				$feedName = urldecode($feedName);
				$result = $this->api_model->get_feedID($feedName);
				$returnedData = $baseURL.'entries/'.$result[0]['id'];
				echo $returnedData;
        		break;
    		
    		case 'create':
        		$baseURL = base_url().'create/';
        		
        		if($feedName == "new")
				{
					echo $baseURL.'feed';
					break;
				}
				else
				{
					$feedName = urldecode($feedName);
					$result = $this->api_model->get_feedID($feedName);
					$returnedData = $baseURL.'entry/'.$result[0]['id'];
					echo $returnedData;
        			break;
				}
    		}
		}
	
	
	function feedEntries()
		{
		$returnedData = $this->feeds_model->get_entries($this->uri->segment(3));
		$data = array(
			'code'		=>	200,
			'data'		=>	$returnedData,
			'format'	=>	$this->uri->segment(4),
			'counter'	=>	0
			);
		$this->response($data);
		}
	
	function subscribersSuggest()
		{
		$email = urldecode($this->uri->segment(3));
		$returnedData = $this->subscribers_model->subscribers_suggest($email);
		$data = array(
			'code'		=>	200,
			'data'		=>	$returnedData,
			'format'	=>	$this->uri->segment(4),
			'counter'	=>	0
			);
		$this->response($data);
		}
	
	
	function mobile2()
		{
			$authorID = $this->uri->segment(3);
			$feedID = $this->uri->segment(4);
			
			if(!$this->uri->segment(5))
			{
				
				$queryResponse = $this->api_model->get_feedEntries($authorID, $feedID);
				$formattedResponse = array();
				
				foreach($queryResponse as $posts)
				{
					$authorName = $this->api_model->get_authorName($posts['author_id']);
					$temp = array(
						'id'		=>	$posts['id'],
						'url'		=>	base_url('api/mobile').'/'.$posts['author_id'].'/'.$posts['feed_id'].'/'.$posts['id'],
						'title'		=>	$posts['title'],
						'date'		=>	$posts['publish_date'],
						'author'	=>	$authorName->first_name.' '.$authorName->last_name,
						'thumbnail'	=>	'http://placehold.it/150x150'
						);
					
					array_push($formattedResponse, $temp);
				}
				
				$returnedData = array(
					'status'		=> 'ok',
					'count'			=> 10,
					'count_total'	=> 1470,
					'pages'			=> 147,
					'posts'			=> $formattedResponse
					);
				$this->output
					->set_content_type('application/json')
					->set_output(json_encode($returnedData));
			}
			else //a specific article is requested and must be delivered via a view
			{
				//look up article based on authorID, feedID, and articleID
				$authorName = $this->api_model->get_article($authorID,$feedID,$this->uri->segment(5));
				$this->output->set_output('<br /><br /><br /><h2>'.$authorName->title.'</h2>'.$authorName->body);
				
			}
			
		}
	
	function mobile()
		{
			date_default_timezone_set('America/Detroit');
			//2010-10-15 10:00:00
			$dateTimeNow = date('Y-m-d G:i:s');
			
			$formattedResponse = array();
			
			switch($this->uri->segment(3))
				{
				case "feed":
					// get articles for a feed (limit 10)
					$queryResponse = $this->api_model->feed_EntriesByID($this->uri->segment(4), $dateTimeNow);
					
					foreach($queryResponse as $posts)
						{
						$authorName = $this->api_model->get_authorName($posts['author_id']);
						$temp = array(
							'id'		=>	$posts['id'],
							'url'		=>	base_url('api/mobile').'/'.$posts['id'],
							'title'		=>	$posts['title'],
							'date'		=>	$posts['publish_date'],
							'author'	=>	$authorName->first_name.' '.$authorName->last_name,
							'thumbnail'	=>	'http://placehold.it/150x150'
							);
						
						array_push($formattedResponse, $temp);
						}
					
					$returnedData = array(
						'status'		=> 'ok',
						'count'			=> count($formattedResponse),
						'count_total'	=> 1470,
						'pages'			=> 147,
						'posts'			=> $formattedResponse
						);
					$this->output
						->set_content_type('application/json')
						->set_output(json_encode($returnedData));
					
					break;
				
				case "author":
					// get all articles from author (limit 10)
					$queryResponse = $this->api_model->feed_EntriesByAuthor($this->uri->segment(4), $dateTimeNow);
					
					foreach($queryResponse as $posts)
						{
						$authorName = $this->api_model->get_authorName($posts['author_id']);
						$temp = array(
							'id'		=>	$posts['id'],
							'url'		=>	base_url('api/mobile').'/'.$posts['id'],
							'title'		=>	$posts['title'],
							'date'		=>	$posts['publish_date'],
							'author'	=>	$authorName->first_name.' '.$authorName->last_name,
							'thumbnail'	=>	'http://placehold.it/150x150'
							);
						
						array_push($formattedResponse, $temp);
						}
					
					$returnedData = array(
						'status'		=> 'ok',
						'count'			=> count($formattedResponse),
						'count_total'	=> 1470,
						'pages'			=> 147,
						'posts'			=> $formattedResponse
						);
					$this->output
						->set_content_type('application/json')
						->set_output(json_encode($returnedData));
					break;
				
				default:
					//look up article based on entryID and publish date
					$template = 
					'<!DOCTYPE html>'
					.'<html>'
					.'	<head>'
					.'		<style>'
					.'			body {background-image:url("'.base_url().'/resource/img/post-pg.png"); margin-left: 50px;}'
					.'		</style>'
					.'	</head>'

					.'	<body>';
					

					$article = $this->api_model->feed_EntryByID($this->uri->segment(3), $dateTimeNow);
					$this->output->set_output($template.'<br /><br /><br /><h2>'.$article->title.'</h2>'.$article->body.'</body></html>');
				}
		}
	
	function response($response)
		{
		//if there is a response
		if(isset($response) && $response['code'] == 200)
			{
			if(isset($response['data']))
				{
				switch($response['format'])
					{
					//Encodes and outputs in xml
					case 'xml':
						$this->load->dbutil();
						$xmlOutput = $this->dbutil->xml_from_result($response['data']);
						$this->output
							->set_content_type('text/xml')
							->set_output($xmlOutput);
						break;
					
					//Encodes and outputs a html table
					case 'html':
						//send data array to a view()
						//create an html table out of the data array
						//print_r($response['data']->result_array());
						$this->load->view('api-view',$response);
						break;
					
					//Encodes and outputs in json - This is the default action
					default:
						$this->output
							->set_content_type('application/json')
							->set_output(json_encode($response['data']->result_array()));
					}
				}
			//send appropriate response code 200, 201, ect...
			}
		else
			{
			//Generic error - requested object not found
			show_404();
			}
		}
	}