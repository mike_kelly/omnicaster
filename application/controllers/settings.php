<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller{
	
	public function index()
	{
		
		//print_r($this->session->all_userdata());
		$settingsChange = 0;
		
		$result = $this->settings_model->get_settings();
		
		
		if($this->input->post('inputEmail') !=$result->email && $this->input->post('inputEmail') != '')
		{
			//update setting information 
			//databaseFieldName, sessionFieldName, replacementValue
			$this->settings_model->update_settings('email', 'email',$this->input->post('inputEmail'));
			
			$settingsChange = 1;
		}
		
		if($this->input->post('inputFirstName') !=$result->first_name && $this->input->post('inputFirstName') != '')
		{
			//update setting information
			//$this->settings_model->update_settings('first_name', 'email',$this->input->post('inputEmail'));
			$settingsChange = 1;
		}
		
		if($this->input->post('inputLastName') !=$result->last_name && $this->input->post('inputLastName') != '')
		{
			//update setting information
			//$this->settings_model->update_settings('last_name', 'email',$this->input->post('inputEmail'));
			$settingsChange = 1;
		}
		
		//echo $settingsChange;
		redirect($this->input->post('sourceURL'), 'refresh');
		
	}

	function passChange()
	{
		
	}
}

/* End of file test.php */
/* Location: ./application/controllers */