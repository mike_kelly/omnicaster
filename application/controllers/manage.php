<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller{
	
	public function subscribers()
	{
		if ($this->ion_auth->logged_in())
			{	
				//print_r($this->session->all_userdata());
				$data = array(
					'title'		 	=> 	'Manage Subscribers',
					'menuSelect'	=>	'subscribers',
					'script'		=>	'subscribers-script'
					);
				
				$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
				$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
				//print_r($data['userdata']);
				
				//echo $data['userdata'][0]['first_name'].' '.$data['userdata'][0]['last_name'];
				
				$this->load->view('template/head', $data);
				$this->load->view('template/navbar');
				$this->load->view('subscribers-view', $data);
				$this->load->view('template/foot');
			}
		else{redirect('auth/login', 'refresh');}
	}
	
	public function feeds()
	{
		//feeds that are looked up by author_ID
		if ($this->ion_auth->logged_in())
			{
				$data = array(
					'title'		 	=> 	'Manage Feeds',
					'menuSelect'	=>	'feeds',
					'script'		=>	'feeds-script',
					'count'			=>	0
					);
				
				$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
				$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
				
				//Begin building final feeds array
				$processedResult = array(
					'author_id'			=> 	$this->session->userdata('author_id'),
					'publication_id'	=>	$this->session->userdata('publication_id')
					);
				
				//get a list of all the feeds
				$feedResults = $this->feeds_model->get_feeds($this->session->userdata('author_id'),$this->session->userdata('publication_id'));
				
				//define child array for feed entries
				$processedResult['feeds'] = array();
				foreach($feedResults as $feed)
					{
						//assign top level feed attributes name and description	
						$processedResult['feeds'][$feed['id']] = array(
							'name'			=>	$feed['name'],
							'description'	=>	$feed['description'],
							'entries'		=>	array()							
							);
					
						//query for entries for this specific feed
						$result = $this->feeds_model->get_entries($feed['id']);
						$entries = $result->result_array();
						if(count($entries)== 0)
							{$processedResult['feeds'][$feed['id']]['entries'] = 0;}
						else 
							{
								//for each entry add to the final result array
								foreach($entries as $subEntries)
									{
										$processedResult['feeds'][$feed['id']]['entries'][$subEntries['id']]['id'] 		= $subEntries['id'];
										$processedResult['feeds'][$feed['id']]['entries'][$subEntries['id']]['title']	= $subEntries['title'];
										$processedResult['feeds'][$feed['id']]['entries'][$subEntries['id']]['body'] 	= $subEntries['body'];
									}
							}
					} // end of looping
		
				$data['publication'] = $processedResult;
				
				
				$this->load->view('template/head', $data);
				$this->load->view('template/navbar', $data);
				$this->load->view('feeds-view', $data);
				$this->load->view('template/foot');
			}
		else{redirect('auth/login', 'refresh');}
		
		
	}
	
	public function entries()
		{
		//To manage entries for a specific feed, feedID must be given as the 3rd uri segment
		// {baseURL}/manage/entries/{feedID}
		
		if($this->uri->segment(3) == ''){redirect('/');}
		
		//Edit individual entries
		elseif($this->uri->segment(3) == 'edit')
			{
			if ($this->ion_auth->logged_in())
				{
				$data = array(
					'menuSelect'	=>	'feeds',
					'script'		=>	'entries-edit-script',
					'userdata'		=>	$this->userinfo_model->getUser_info($this->session->userdata('user_id')),
					'entry'			=>	$this->uri->segment(4),
					);
				
				//Check entry authorization
				//$segments[0] is feedID and $segments[1] is entryID
				$segments = explode("-", $this->uri->segment(4));
				$authorization = $this->feeds_model->feed_authorization($segments[0], $this->session->userdata('publication_id'), $this->session->userdata('author_id'));
				
				if($authorization)
					{
					//function get_entries($feedID, $returnLimit = 'none', $offset = 0)
					//retreive the requested entry, I believe the using an offset will stop working once multiple users are present
					//probably best to make a new model
					$entry = $this->feeds_model->get_entry($segments[0], $segments[1]);
					
					if($entry->num_rows() > 0)
						{
						
						$data['entry_data'] = $entry->result_array();
						$data['title'] = 'Edit: '.$data['entry_data'][0]['title'];
						
						$this->load->view('template/head', $data);
						$this->load->view('template/navbar', $data);
						$this->load->view('entries-edit-view', $data);
						$this->load->view('template/foot');
						}
					else{echo 'Entry not found!';}
					}
				else{echo 'You are not authorized to edit this entry!';}
				
				}
			else{redirect('auth/login', 'refresh');}
			}
		
		else{
			$data = array(
				'title'		 	=> 	'Manage Feeds',
				'menuSelect'	=>	'feeds',
				'script'		=>	'entries-script'
				);
		
			$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
			
			//get feed id from url - {baseURL}/manage/entries/{feedID} 
			$feedID = $this->uri->segment(3);
		
			//verify the users right to manage feed from session data 	
			$authResults = $this->feeds_model->feed_authorization($feedID, $this->session->userdata('publication_id'), $this->session->userdata('author_id'));
		
			//if the user requests an authorized feed
			if($authResults > 0)
				{
			
				$tmpl = array (
                    'table_open'          => '<table class="table table-striped table-hover table-condensed">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
              		);
				
				$this->table->set_template($tmpl);
				
				//Set table headers
				$this->table->set_heading('','Title','Content','Publish Date','');
				
				
				$config['per_page'] = 10;
				
				$get_feedNameReturn = $this->feeds_model->get_feedName($feedID);
				
				$data['feedTitle'] = $get_feedNameReturn->name;
				
				//Get all feed entries for requested feed
				$feedEntries = $this->feeds_model->get_entries($feedID, $config['per_page'], $this->uri->segment(4));
			
				//set the total rows for pagination purposes
				$config['total_rows'] 		=   $this->feeds_model->get_entries($feedID)->num_rows();
				$config['base_url']			=	base_url().'manage/entries/'.$this->uri->segment(3);
				$config['num_links']		=	20;
				$config['uri_segment'] 		= 	4;
				//formatting the pagination links
				$config['full_tag_open'] 	= '<center><div class="pagination"><ul>';
				$config['prev_tag_open'] 	= '<li>';
				$config['prev_tag_close'] 	= '</li>';
				$config['cur_tag_open'] 	= '<li class="active"><a href="#">';
				$config['cur_tag_close'] 	= '</a></li>';
				$config['num_tag_open'] 	= '<li>';
				$config['num_tag_close'] 	= '</li>';
				$config['next_tag_open'] 	= '<li>';
				$config['next_tag_close'] 	= '</li>';
				$config['full_tag_close'] 	= '</ul></div></center>';
				//////////////////////////////////
				
				//initialize the pagination using $config settings
				$this->pagination->initialize($config);
			
				//Check that the feed has entries to manage
				if($feedEntries->num_rows() > 0)
					{
					$entries = $feedEntries->result_array();
					
					$data['entries'] = array();
					$counter = 0;
					
					foreach($entries as $row)
						{
						
						$content = $this->process->entry_preview($row['id']);
						
						//if $content is != 0 then user is an author for requested entry and $content is an array
						if($content != 0)
						{
							//limit the number of characters returned for format restriction reasons	
							if(strlen($content['body']) > 50)
								{$strippedBody = substr($content['body'],0,50).'...';}
							
							else{$strippedBody = $content['body'];}
							
							//if the img src variable is set the the entry has an image
							if(isset($content['images'][0]['src']))
								{
									$body = '<div style="float: left; width: 20%;"><img src="'.$content['images'][0]['src'].'" alt="'.$content['images'][0]['alt'].'" height="50" width="50"></div><div style="float: right; width: 80%;">'.$strippedBody.'</div>';
								}
							else{$body = $strippedBody;}
						}
						else{$body = 0;}
						
												
						if(strlen($row['title']) > 20)
							{$title = substr($row['title'],0,20).'...';}

						else{$title = $row['title'];}
						
						if($row['publish_date'] == '0000-00-00 00:00:00'){$publishDate = "none";} else{$publishDate = $row['publish_date'];}
						$temp = array(
							'checkbox'		=>	'<label class="checkbox"><input type="checkbox"></label>',
							'title'			=>	$title,
							'content'		=>	$body,	
							'publish_date'	=>	'<span class="text-center" >'.$publishDate.'</span>',
							'actions'		=>  '<span id="'.$row['feed_id'].'-'.$row['id'].'"class="pull-right"><i alt="Edit Post" title="Edit Post" class="icon-edit edit"></i> | <i alt="Delete Post" title="Delete Post" class="icon-remove delete"></i></span>'
							);
							
						$data['entries'][$counter] = $temp;
						$counter++;
						 
						}
				
					$this->load->view('template/head', $data);
					$this->load->view('template/navbar', $data);
					$this->load->view('entries-view', $data);
					$this->load->view('template/foot');
					
					}

				//redirect user to create a new entry and flash data error of no entries found for selected feed
				else{ echo "no entries found";}
			
				}
			//user not authorized to manage requested entries
			//redirect to manage feeds page and flash not authorized message
			else {echo "you are not authorized";}
			}
	} //end of entries function
}

/* End of file manage.php */
/* Location: ./application/controllers/manage.php */