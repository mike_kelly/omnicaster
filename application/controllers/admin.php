<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/admin
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		if ($this->ion_auth->logged_in())
			{
				
				$data = array(
					'title' 		=> 	'Admin Control Panel',
					'menuSelect'	=>	'main',
					'script'		=>	'admin-script'
					);	
				
				$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
				$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
				
				$this->session->set_userdata(array('publication_id' => $data['publicationdata'][0]['publication_id']));
				$this->session->set_userdata(array('author_id' => $data['userdata'][0]['author_id']));	
				
				$publication = $this->userinfo_model->get_publicationInfo($this->session->userdata('publication_id'));
				$this->session->set_userdata(array('publication' => $publication[0]['name']));
				
				//$this->load->view('template/head', $data);
				//$this->load->view('template/navbar');
				//$this->load->view('admin-view');
				//$this->load->view('template/foot');
				
				redirect('manage/feeds', 'refresh');	
				
			}
			
		else{redirect('auth/login', 'refresh');}
		
	}
	function userdata()
	{
		$data = array('title' => 'userdata');
		
		
		print_r($this->userinfo_model->getUser_info($this->session->userdata('user_id')));
		echo '<p />';
		print_r($this->session->all_userdata());
		echo '<p />';
		print_r($this->userinfo_model->get_publicationsID($this->session->userdata('user_id')));
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */