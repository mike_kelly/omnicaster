<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscribers extends MY_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/admin
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->ion_auth->logged_in())
			{	
				$data = array(
					'title'		 	=> 	'Manage Subscribers',
					'menuSelect'	=>	'subscribers',
					'script'		=>	'subscribers-script'
					);
				
				$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
				
				//Returns the feed id based on feed name
				$feedID = $this->feeds_model->get_feed($this->session->userdata('author_id'),$this->input->post('feed-name'));
				
				//now search the subscribers table for individuals that have subscribed to the current working feed 
				$subscriberList = $this->subscribers_model->get_subscribers($feedID[0]['id']);
				
				echo 'Subscribers from feed #'.$feedID[0]['id'].'<br /><br />';
				
				
				foreach($subscriberList as $subscriber)
					{
						echo $subscriber['email'].'<br />';
					}
				
				//echo $data['userdata'][0]['first_name'].' '.$data['userdata'][0]['last_name'];
				
				//$this->load->view('template/head', $data);
				//$this->load->view('template/navbar');
				//$this->load->view('subscribers-view', $data);
				//$this->load->view('template/foot');
			}
		else{redirect('auth/login', 'refresh');}
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */