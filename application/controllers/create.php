<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create extends MY_Controller{
	
	function feed()
	{
		echo "much success feed creator!";
		
	}
	
	function entry($feedID)
	{
		$data = array(
			'title'		 	=> 	'New Entry',
			'menuSelect'	=>	'feeds',
			'script'		=>	'entries-new-script'
			);
			
		//echo 'success '.$feedID;
		
		$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
		//get feed id from url - {baseURL}/manage/entries/{feedID} 
		$data['feedID'] = $this->uri->segment(3);
		$get_feedNameReturn = $this->feeds_model->get_feedName($data['feedID']);
		$data['feedTitle'] = $get_feedNameReturn->name;
		
		//verify the users right to manage feed from session data 	
		$authResults = $this->feeds_model->feed_authorization($feedID, $this->session->userdata('publication_id'), $this->session->userdata('author_id'));
		
		//if the user requests an authorized feed
		if($authResults > 0)
			{
			$this->load->view('template/head',$data);
			$this->load->view('template/navbar',$data);
			$this->load->view('entries-new-view',$data);
			$this->load->view('template/foot',$data);
			}
		else{
			echo "fail!";
			}
	}
}

/* End of file create.php */
/* Location: ./application/controllers/create.php */