<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Save extends MY_Controller{
	
	public function entry()
	{
	 //determine if post is set
	 if ($_SERVER['REQUEST_METHOD'] === 'POST')
	 	{
    	// make sure entry exists
    	//update the entry with new data
    		//id
    		//title
    		//body
    		//feed_id
    		//publish_date
			
			
			$input = array(
				'id'			=>	$this->input->post('entryid'),
				'title'			=>	$this->input->post('entrytitle'),
				'body'			=>	$this->input->post('entrybody'),
				'thumbnail'		=>	'http://placehold.it/150x150',
				'feed_id'		=>	$this->input->post('feedid'),
				'publish_date'	=>	$this->input->post('pubdate')
				);
			
			//print_r($input);
			
			//$this->entries_model->update_entry($feed_id, $entry_id, $data;)
			$this->entries_model->update_entry($input['feed_id'], $input['id'], $input);
			
			redirect('manage/entries/'.$input['feed_id'], 'refresh');
			
	 	}
	 else{echo "Entry edits must be submited as POST.";}
	 
	 
	}
	
	function newEntry()
	{
		//determine if post is set
	 if ($_SERVER['REQUEST_METHOD'] === 'POST')
	 	{
    	// make sure entry exists
    	//update the entry with new data
    		//id
    		//title
    		//body
    		//feed_id
    		//publish_date
			
			
			$input = array(
				'title'			=>	$this->input->post('entrytitle'),
				'body'			=>	$this->input->post('entrybody'),
				'thumbnail'		=>	'http://placehold.it/150x150',
				'feed_id'		=>	$this->input->post('feedid'),
				'author_id'		=>	$this->input->post('authorid'),
				'publish_date'	=>	$this->input->post('pubdate')
				);
			
			//print_r($input);
			
			//$this->entries_model->update_entry($feed_id, $entry_id, $data;)
			$return = $this->entries_model->create_entry($input);
			
			
			if($return > 0)
			{
				redirect('manage/entries/'.$input['feed_id'], 'refresh');
			}
			else{
				echo "there was an error";
			}
			
	 	}
	 else{echo "Entry edits must be submited as POST.";}
	}
	
}
		