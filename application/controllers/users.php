<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller{
	
	public function index()
		{
		if ($this->ion_auth->logged_in())
			{
			
			$data = array(
				'title'		 	=> 	'Manage Users',
				'menuSelect'	=>	'users',
				'script'		=>	'users-script'
				);
			
			$data['userdata'] = $this->userinfo_model->getUser_info($this->session->userdata('user_id'));
			$data['publicationdata'] = $this->userinfo_model->get_publicationsID($data['userdata'][0]['user_id']);
			
			$this->load->view('template/head', $data);
			$this->load->view('template/navbar');
			$this->load->view('users-view', $data);
			$this->load->view('template/foot');
			}
		
		else{redirect('auth/login', 'refresh');}
		
		}
	
	public function pubUsers()
		{
			echo "User Success";
		}
	
	public function confirmation($action, $email, $md5Time)
		{
			$email = urldecode($email);
			//check database for the invite record
			$invitee = $this->invites_model->lookupInvitee($email, $md5Time);
			echo count($invitee);
			switch ($action) {
				case 'accept':
					echo 'Invitee Email: '.$invitee->email.'<br />';
					echo 'Inviter Email: '.$invitee->inviterEmail.'<br />';
					echo 'Invite Hash: '.$invitee->inviteHash.'<br />';
					echo 'Invite Time: '.$invitee->inviteTime.'<br />';
					echo 'Add to Publication: '.$invitee->addToPub;
					
					if (count($invitee) == 1)
					{
						
					}
						//if found
						//if add to publication
					//display create new user view
					//create user and add to publication
				// else
					// create user and create publication
			//else
				// show error that no invite record was found
			
			
			
			
					break;
				
				case 'decline':
					
					echo $email.' declined to join at this time';
					
					break;
				
				default:
					
					redirect('/', 'refresh');
					
					break;
			}
		}
	
	//creates the invitee record and sends out the confirmation email
	public function invitees()
		{
		if ($this->ion_auth->logged_in())
			{
				//get userid and publicationid
				$userID = $this->session->userdata('user_id');
				$pubID = $this->session->userdata('publication_id');
				$inviteHash = md5(date('Y-m-d G:i:s'));
				//email, timedate ('YYYY-MM-DD HH:MM:SS'), userID, publicationID, addToPublication
				$data = array(
   					'email' 		=> $this->input->post("email"),
   					'inviteTime'	=> date('Y-m-d G:i:s'),
   					'userID'	 	=> $userID,
   					'pubID'			=> $pubID,
   					'addToPub'		=> $this->input->post("addToPub"),
   					'inviterEmail'	=> $this->session->userdata('email'),
   					'inviteHash'	=> $inviteHash
					);
				
				//Check if email was a valid entry
				//This email check should also be done with javascript to catch it on the front end
				//if valid do the following
					
					//insert invite record into table
					$insertResult = $this->invites_model->put_invitees($data);
					
					//if there was an affected row
					if($insertResult > 0)
						{
							//send invite email to $data['email']
							$inviteString = base_url("users/confirmation/accept").'/'.urlencode($data['email']).'/'.$inviteHash;
							$declineString = base_url("users/confirmation/decline").'/'.urlencode($data['email']).'/'.$inviteHash;
							
							$body = "<h2>You have been invited to be an Omnicaster Author!</h2>"
								
								."<p>"
								."Click on the following link to confirm your membership. If the link does not appear correctly or is not click-able "
								."copy and paste the link into your browser.<br />"
								.'{unwrap}<strong><a href="'.$inviteString.'">'.$inviteString.'</a></strong>{/unwrap}'
								."</p>"
								."<p>"
								."If you do not wish to participate and refuse this invitation click on the following link and the sender will be notified.<br />"
								.'{unwrap}<strong><a href="'.$declineString.'">'.$declineString.'</a></strong>{/unwrap}'
								."</p>"
								
								."<p>Thank you,</p>"
								
								."<p><strong>The Omnicaster Team</strong></p>";		 
							
							
							$this->load->library('email');
							
							$this->email->from('admin@omnicaster.com', 'Mike Kelly');
							$this->email->to($data['email']);
							
							$this->email->subject('Omnicaster Author Invitation');
							$this->email->message($body);	
							
							$this->email->send();
							
							//echo "<h4>Record was inserted successfully</h4>";
							//print_r($data);
							
							echo 
							'<p>'
    						.'	<span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>'
    						.'	<div style="margin-left: 50px;">'
    						.'   Invitation has been successfully created and sent to <strong>'.$data['email'].'</strong>.'
  							.'   <br />'
  							.'   <br />'
    						.'	 You will be notified when action is taken by the invitee via email.'
    						.'  </div>'
  							.'</p>';
							
							//echo $this->email->print_debugger();
							
						}
					else
						{
							//set flash message: "Invite generation fail."
							echo "record insert fail";
						}
				
				//if email is not valid return error
			
			}

		else{redirect('auth/login', 'refresh');}
		
		}
		
	public function getInvites()
		{
		if ($this->ion_auth->logged_in())
			{
			//get userid and publicationid
			$userID = $this->session->userdata('user_id');
			$pubID = $this->session->userdata('publication_id');
				
			//query for invites
			//query where user_id and publication_id match
			$invitees = $this->invites_model->get_invitees($userID, $pubID);
			
			//if invites are not == to 0
			//generate the table for ajax success output
			if($invitees->num_rows() > 0)
				{
				//return table of user invites
				$invitees->result_array();
				
				
				echo '<table class="table table-hover table-bordered">';
				echo '	<tr>';
				echo '		<th>Email</th>';
				echo '		<th>Add to Publication</th>';
				echo '		<th>Invitation Sent</th>';
				echo '	</tr>';
				
				foreach($invitees->result_array() as $results)
					{
					echo '<tr>';
					echo '<td>'.$results['email'].'</td>';
					if($results['addToPub'] == 1) {echo '<td>Yes</td>';} else {echo '<td>No</td>';}
					
					$date1 = new DateTime($results['inviteTime']);
					$date2 = new DateTime();
					
					$interval = $date1->diff($date2);
					//Y-m-d G:i:s
					//echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 
					if($interval->days != 0){if($interval->days > 1){echo '<td>'.$interval->days.' days ago</td>';}else{echo '<td>'.$interval->days.' day ago</td>';}}
					elseif($interval->h != 0){if($interval->h >1){echo '<td>'.$interval->h.' hours ago</td>';}else{echo '<td>'.$interval->h.' hour ago</td>';}}
					elseif($interval->i != 0){if($interval->i >1){echo '<td>'.$interval->i.' minutes ago</td>';}else{echo '<td>'.$interval->i.' minute ago</td>';}}
					else{echo '<td>moments ago</td>';}
					
					echo '</tr>';
					}
				
				echo '</table>';
				}
				
			//if invites are == to 0
			//return no invites found
			else
				{echo "You have no invites on record.";}	
			}

		else{redirect('auth/login', 'refresh');}
		}

	}

/* End of file users.php */
/* Location: ./application/controllers/users.php */