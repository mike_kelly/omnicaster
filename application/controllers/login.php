<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		session_start();
		$inactive = 600;
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
		//$2a$07$somesillystringforsaleGtSjxCyW2FqSWixEfAAhSCg0ye7V0ze
		
		if(isset($_SESSION['logged_in'])){redirect(base_url(),'refresh');}
		else
			{
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('template/head');
					$this->load->view('login-view');
					$this->load->view('template/foot');
				}
				else
				{
				$result = $this->login_model->validate_login($this->input->post('email'),crypt($this->input->post('password'), '$2a$07$1o4esilWystrfng1oDs0q1'));
				//echo $this->input->post('email').'<br />';
				//echo crypt($this->input->post('password'), '$2a$07$1o4esilWystrfng1oDs0q1');
				
				if($result == 1)
					{
					$_SESSION['logged_in'] = TRUE;
						
					// check to see if $_SESSION["timeout"] is set
					if (isset($_SESSION["timeout"])) {
					    // calculate the session's "time to live"
					    $sessionTTL = time() - $_SESSION["timeout"];
					    if ($sessionTTL > $inactive) {
							redirect(base_url('router/logout'), 'refresh');
					    	}
						}
					
					$_SESSION["timeout"] = time();
					
					echo $_SESSION['logged_in'];
					}
				else
					{
						//Login wasn't successfull redirect to logout
						redirect(base_url('router/logout'), 'refresh');
					}
				}
			}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */